
# Figure 3 c

# Comparison of motif similarity at embryonic stages.
To assess the similarity of developmental stages in the two species based on what TFs are important in them.    

We counted motifs (PWMs) in dynamic ATAC peaks of our various developmental stages. We divided each motif's count by the total hits of the motif in all stages and then scaled the values in each stage.     

We could then calculate the correlation between stages and thus, we created a heatmap of correlation for the developmental stages of the two species.

# Preparation


```python
from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
import matplotlib
from matplotlib import pyplot as plt


import pandas as pd
from scipy.stats import pearsonr
from collections import Counter
from sklearn import preprocessing

import warnings
warnings.filterwarnings('ignore')
```

### Motif families

We have a large set of Position Weight Matrices, which we group together by their protein
e.g. AP-2_Average_15 and AP-2_Average_17 are different position weight matrices,
but both assigned to the 'Tfap2e' transcription factor so we will consider all instances
of these two PWMs as instances of Tfap2e.    
     
Another example, are ARID_BRIGHT_RFX_Average_2,ARID_BRIGHT_RFX_Average_3 and ARID_BRIGHT_RFX_M4343_1.02 :    
These 3 PWMs all are assigned to the TFs "Rfx1;Rfx2;Rfx4;Rfx7" so we will consider all instances of those 3 PWMs    
to be instances of a "Rfx1;Rfx2;Rfx4;Rfx7" protein family.
    
To accomplish this, we will make a dictionary that maps PWMs to Protein family names :


```python
# This table connects PWMs to gene families
superfams = pd.read_csv("./data/PWMname_to_ProteinFamily.tsv.gz", sep='\t', header=None)

superfams.columns = ['PWMname','GFid','GFname']
superfams.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PWMname</th>
      <th>GFid</th>
      <th>GFname</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>AP-2_Average_15</td>
      <td>17068</td>
      <td>Tfap2e</td>
    </tr>
    <tr>
      <th>1</th>
      <td>AP-2_Average_17</td>
      <td>17068</td>
      <td>Tfap2e</td>
    </tr>
    <tr>
      <th>2</th>
      <td>AP-2_Average_26</td>
      <td>17068</td>
      <td>Tfap2e</td>
    </tr>
    <tr>
      <th>3</th>
      <td>AP-2_Average_27</td>
      <td>17068</td>
      <td>Tfap2e</td>
    </tr>
    <tr>
      <th>4</th>
      <td>AP-2_M6146_1.02</td>
      <td>17068</td>
      <td>Tfap2e</td>
    </tr>
  </tbody>
</table>
</div>




```python
# the table needs a little fixing:    
# some families of multiple proteins are identical,
# but their IDs are in different order, so they can't be recognized as identical.
# we'll go through the table and sort all name with multiple IDs
# in order to make identical families have identical names.
def order_things(x):
    return ';'.join(sorted(x.split(';')))

lot = []
for i,row in superfams.iterrows():
    if ';' not in row['GFid']:
        lot.append( row.tolist() )
    else:
        a,b = [';'.join(jj) for jj in zip(*sorted(zip(row['GFid'].split(';'), row['GFname'].split(';')), key= lambda x: int(x[0]) ))]
        lot.append( [row['PWMname'], a, b] )
        
superfams_ = pd.DataFrame(lot)

# SFD here, maps the PWM names to unique protein numbers:
# 'AP-2_Average_15': '17068'
SFD = dict(superfams_[[0,1]].to_records(index=False))

# but then in cases like 'ARID_BRIGHT_RFX_Average_2',
# we have multiple proteins: 66;15742;15743;18247
# we will replace this long concatenation of numbers 
# with a unique number:
fuid = {}
c = 0
for v in SFD.values():
    if v not in fuid:
        fuid[v] = c
        c += 1
SFDu = {k:fuid[v] for k,v in SFD.items()}
```


```python
list(SFDu.items())[:5]
```




    [('T-box_Average_6', 0),
     ('T-box_Average_1', 1),
     ('Homeodomain_Average_495', 2),
     ('Forkhead_Average_39', 3),
     ('Homeodomain_M4692_1.02', 4)]



### The mapped motifs:


```python
dan_motif_bed = BT("./data/danre_pwm_hits.bed.gz")
bla_motif_bed = BT("./data/bralan_pwm_hits.bed.gz")
bla_motif_bed.head(2)
```

    Sc0000000	5593	5598	Homeodomain_Average_626	6.60757208173	+
     Sc0000000	5695	5707	C2H2_ZF_Average_154	10.8804149236	+
     

### The ATACseq peaks

We have compiled a set of peaks that turn ON or OFF in a reasonable pattern during development,
and called these peaks 'dynamic'.     
    
Per stage we will only consider these dynamic peaks.     
     
We load them in pyBedTool objects, BT().     
      
We load the stages from each stage, and keep the ones that have been marked 
as logicaldynamic


```python
# some helper functions to get the dynamic peaks per stage

a_dynamic = BT("./data/atac_peaks/amphi_logicaldynamic_idr.bed.gz")
def bla_stagepeaks(stage):
    sd = BT("./data/atac_peaks/amphi_{}_idrpeaks.bed.gz".format(stage))
    sd = sd.intersect(a_dynamic,u=True, nonamecheck=True)  
    return sd.sort()

z_dynamic = BT("./data/atac_peaks/zebra_danRer10_logicaldynamic_idr.bed.gz")
def dan_stagepeaks(stage):
    sd = BT("./data/atac_peaks/zebra_danRer10_{}_idrpeaks.bed.gz".format(stage))
    sd = sd.intersect(z_dynamic,u=True, nonamecheck=True)
    return sd.sort()

amphi_stages = ['8','15','36','60','hep']
zebra_stages = ['dome','shield','80epi','8som','24h','48h']

bla_peaks = [bla_stagepeaks(stage) for stage in amphi_stages]
dan_peaks = [dan_stagepeaks(stage) for stage in zebra_stages]
```


```python
bla_peaks[0].head(3)
```

    Sc0000000	311943	312518
     Sc0000000	319117	319493
     Sc0000000	356649	356918
     


```python
TFids = sorted(set(SFDu.values()))
```


```python
# Here we get a dictionary, mapping the TF unique numbers to a total count per organism
# We will use this to normalize later on
_temp = (BT("./data/atac_peaks/zebra_danRer10_merged_idrpeaks.bed.gz")                # load the peaks
        .sort()                                                              # sort them
        .intersect(dan_motif_bed, loj=True, sorted=True, nonamecheck=True)   # left-outer-merge with the PWMs
        .to_dataframe())                                                     # make into a dataframe

# we then map the SFDu dictionary on the column carrying the PWM names, drop empty rows,
# and get a count (with Counter, a handy python extension of dictionaries)
# of each unique ID
dan_totals = Counter(_temp['thickStart'].map(SFDu).dropna().astype(int))

_temp = (BT("./data/atac_peaks/amphi_merged_idrpeaks.bed.gz")
        .sort()
        .intersect(bla_motif_bed, loj=True, sorted=True, nonamecheck=True)
        .to_dataframe())
bla_totals = Counter(_temp['thickStart'].map(SFDu).dropna().astype(int))

# dan_totals ~= {0: 3653,
#  1: 16725,
#  2: 1848,
#  3: 11946
#  ....}

# we cast the total counts in a list with a proper orger:
dan_normalizer = [dan_totals.get(x,1) for x in TFids]
bla_normalizer = [bla_totals.get(x,1) for x in TFids]
```


```python
# the resulting DataFrame after the LOJ operation.
# for each ATAC peak,we get all its intersections with PWMs
_temp.head(2)
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>thickStart</th>
      <th>thickEnd</th>
      <th>itemRgb</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Sc0000000</td>
      <td>5589</td>
      <td>5975</td>
      <td>Sc0000000</td>
      <td>5593</td>
      <td>5598</td>
      <td>Homeodomain_Average_626</td>
      <td>6.607572</td>
      <td>+</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Sc0000000</td>
      <td>5589</td>
      <td>5975</td>
      <td>Sc0000000</td>
      <td>5695</td>
      <td>5707</td>
      <td>C2H2_ZF_Average_154</td>
      <td>10.880415</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
#dan_totals is a dictionary mapping the uniqueIds of PWMs to counts
list(dan_totals.items())[:3]
```




    [(0, 3653), (1, 16725), (2, 1848)]




```python
# The "normalizer" lists are derived from the dictionaries and are in order of "TFids"
bla_normalizer[:3]
```




    [1188, 3585, 930]



###  Mapping the motifs to peaks:    
     
We did this already to get the normalization counts, now we will do it 
with the stage dynamic peaks


```python
# Here we make a dataframe per stage per organism
# Each DataFrame corresponds to one stage and maps motifs to the peaks that were active on
# that stage. We use the LEFT OUTER JOIN function of bedtools to join motifs to peaks:
bla_lojs = [(stage
                .sort()
                .intersect(bla_motif_bed, sorted=True, loj=True, nonamecheck=True)
                .to_dataframe()) for stage in bla_peaks]
dan_lojs = [(stage
                .sort()
                .intersect(dan_motif_bed, sorted=True, loj=True, nonamecheck=True)
                .to_dataframe()) for stage in dan_peaks]
```


```python
# For example, the DF for the first stage of amphioxus:
bla_lojs[0].head(2)
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>thickStart</th>
      <th>thickEnd</th>
      <th>itemRgb</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Sc0000000</td>
      <td>311943</td>
      <td>312518</td>
      <td>Sc0000000</td>
      <td>311942</td>
      <td>311946</td>
      <td>C2H2_ZF_Average_177</td>
      <td>5.460094</td>
      <td>+</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Sc0000000</td>
      <td>311943</td>
      <td>312518</td>
      <td>Sc0000000</td>
      <td>312028</td>
      <td>312035</td>
      <td>Grainyhead_M6529_1.02</td>
      <td>7.510921</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Finally, we make a DF with counts for each TF in each stage:
# These are the counts in the dynamic peaks
ddf = pd.DataFrame(TFids)
ddf.columns = ['fam']

for en,stage in enumerate(zebra_stages):
    ddf[stage] = ddf['fam'].map(Counter(dan_lojs[en]['thickStart'].dropna().map(SFDu).dropna().astype(int)))
    
ddf.set_index('fam', inplace=True, drop=True)

ddf = ddf.T.fillna(1)
ddf
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>fam</th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>...</th>
      <th>247</th>
      <th>248</th>
      <th>249</th>
      <th>250</th>
      <th>251</th>
      <th>252</th>
      <th>253</th>
      <th>254</th>
      <th>255</th>
      <th>256</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>dome</th>
      <td>161</td>
      <td>729</td>
      <td>74</td>
      <td>389</td>
      <td>178</td>
      <td>854</td>
      <td>1924</td>
      <td>701</td>
      <td>140</td>
      <td>2609</td>
      <td>...</td>
      <td>38</td>
      <td>4441</td>
      <td>50</td>
      <td>166</td>
      <td>452</td>
      <td>91</td>
      <td>142</td>
      <td>0</td>
      <td>566</td>
      <td>1004</td>
    </tr>
    <tr>
      <th>shield</th>
      <td>409</td>
      <td>1931</td>
      <td>165</td>
      <td>1014</td>
      <td>419</td>
      <td>2058</td>
      <td>5522</td>
      <td>1855</td>
      <td>336</td>
      <td>7093</td>
      <td>...</td>
      <td>136</td>
      <td>10826</td>
      <td>135</td>
      <td>443</td>
      <td>1241</td>
      <td>176</td>
      <td>471</td>
      <td>0</td>
      <td>1741</td>
      <td>2660</td>
    </tr>
    <tr>
      <th>80epi</th>
      <td>638</td>
      <td>2991</td>
      <td>228</td>
      <td>1657</td>
      <td>708</td>
      <td>3119</td>
      <td>9240</td>
      <td>2965</td>
      <td>520</td>
      <td>11323</td>
      <td>...</td>
      <td>282</td>
      <td>17599</td>
      <td>234</td>
      <td>725</td>
      <td>2060</td>
      <td>260</td>
      <td>846</td>
      <td>0</td>
      <td>3015</td>
      <td>4474</td>
    </tr>
    <tr>
      <th>8som</th>
      <td>756</td>
      <td>3693</td>
      <td>298</td>
      <td>2357</td>
      <td>904</td>
      <td>3708</td>
      <td>12270</td>
      <td>3884</td>
      <td>763</td>
      <td>13939</td>
      <td>...</td>
      <td>402</td>
      <td>23813</td>
      <td>318</td>
      <td>964</td>
      <td>2790</td>
      <td>408</td>
      <td>1236</td>
      <td>0</td>
      <td>4242</td>
      <td>5956</td>
    </tr>
    <tr>
      <th>24h</th>
      <td>785</td>
      <td>3911</td>
      <td>333</td>
      <td>2504</td>
      <td>963</td>
      <td>3775</td>
      <td>13310</td>
      <td>4044</td>
      <td>861</td>
      <td>14794</td>
      <td>...</td>
      <td>450</td>
      <td>26475</td>
      <td>417</td>
      <td>1132</td>
      <td>3176</td>
      <td>487</td>
      <td>1452</td>
      <td>0</td>
      <td>4843</td>
      <td>6715</td>
    </tr>
    <tr>
      <th>48h</th>
      <td>672</td>
      <td>3333</td>
      <td>335</td>
      <td>2185</td>
      <td>775</td>
      <td>3078</td>
      <td>11303</td>
      <td>3355</td>
      <td>785</td>
      <td>12285</td>
      <td>...</td>
      <td>420</td>
      <td>23411</td>
      <td>367</td>
      <td>965</td>
      <td>2712</td>
      <td>433</td>
      <td>1294</td>
      <td>0</td>
      <td>4150</td>
      <td>5916</td>
    </tr>
  </tbody>
</table>
<p>6 rows × 257 columns</p>
</div>




```python
# And the same for amphioxus
bdf = pd.DataFrame(TFids)
bdf.columns = ['fam']
for en,stage in enumerate(amphi_stages):
    bdf[stage] = bdf['fam'].map(Counter(bla_lojs[en]['thickStart'].dropna().map(SFDu).dropna().astype(int)))
    
bdf.set_index('fam', inplace=True, drop=True)

bdf = bdf.T.fillna(1)
bdf
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>fam</th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>...</th>
      <th>247</th>
      <th>248</th>
      <th>249</th>
      <th>250</th>
      <th>251</th>
      <th>252</th>
      <th>253</th>
      <th>254</th>
      <th>255</th>
      <th>256</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>8</th>
      <td>73</td>
      <td>257</td>
      <td>50</td>
      <td>251</td>
      <td>95</td>
      <td>357</td>
      <td>1093</td>
      <td>438</td>
      <td>73</td>
      <td>789</td>
      <td>...</td>
      <td>22</td>
      <td>2330</td>
      <td>52</td>
      <td>76</td>
      <td>223</td>
      <td>26</td>
      <td>94</td>
      <td>0</td>
      <td>307</td>
      <td>593</td>
    </tr>
    <tr>
      <th>15</th>
      <td>196</td>
      <td>618</td>
      <td>172</td>
      <td>707</td>
      <td>224</td>
      <td>885</td>
      <td>2838</td>
      <td>1115</td>
      <td>177</td>
      <td>2482</td>
      <td>...</td>
      <td>72</td>
      <td>6641</td>
      <td>135</td>
      <td>221</td>
      <td>504</td>
      <td>103</td>
      <td>343</td>
      <td>0</td>
      <td>687</td>
      <td>1550</td>
    </tr>
    <tr>
      <th>36</th>
      <td>219</td>
      <td>652</td>
      <td>177</td>
      <td>942</td>
      <td>266</td>
      <td>998</td>
      <td>3234</td>
      <td>1287</td>
      <td>200</td>
      <td>2979</td>
      <td>...</td>
      <td>101</td>
      <td>8168</td>
      <td>153</td>
      <td>275</td>
      <td>588</td>
      <td>140</td>
      <td>500</td>
      <td>0</td>
      <td>766</td>
      <td>2003</td>
    </tr>
    <tr>
      <th>60</th>
      <td>236</td>
      <td>761</td>
      <td>217</td>
      <td>1155</td>
      <td>297</td>
      <td>1082</td>
      <td>3316</td>
      <td>1337</td>
      <td>244</td>
      <td>3079</td>
      <td>...</td>
      <td>123</td>
      <td>10328</td>
      <td>209</td>
      <td>302</td>
      <td>669</td>
      <td>190</td>
      <td>524</td>
      <td>0</td>
      <td>831</td>
      <td>2391</td>
    </tr>
    <tr>
      <th>hep</th>
      <td>212</td>
      <td>608</td>
      <td>144</td>
      <td>998</td>
      <td>260</td>
      <td>897</td>
      <td>2761</td>
      <td>1241</td>
      <td>207</td>
      <td>2389</td>
      <td>...</td>
      <td>77</td>
      <td>8786</td>
      <td>249</td>
      <td>266</td>
      <td>555</td>
      <td>191</td>
      <td>400</td>
      <td>0</td>
      <td>665</td>
      <td>2306</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 257 columns</p>
</div>




```python
# we divide the count in each stage by the total count
bdf = bdf/bla_normalizer
ddf = ddf/dan_normalizer
bdf
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>fam</th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>...</th>
      <th>247</th>
      <th>248</th>
      <th>249</th>
      <th>250</th>
      <th>251</th>
      <th>252</th>
      <th>253</th>
      <th>254</th>
      <th>255</th>
      <th>256</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>8</th>
      <td>0.061448</td>
      <td>0.071688</td>
      <td>0.053763</td>
      <td>0.052129</td>
      <td>0.064189</td>
      <td>0.075524</td>
      <td>0.068462</td>
      <td>0.068246</td>
      <td>0.068224</td>
      <td>0.060133</td>
      <td>...</td>
      <td>0.042553</td>
      <td>0.050174</td>
      <td>0.055556</td>
      <td>0.059006</td>
      <td>0.072473</td>
      <td>0.030806</td>
      <td>0.051450</td>
      <td>0.0</td>
      <td>0.079472</td>
      <td>0.052725</td>
    </tr>
    <tr>
      <th>15</th>
      <td>0.164983</td>
      <td>0.172385</td>
      <td>0.184946</td>
      <td>0.146833</td>
      <td>0.151351</td>
      <td>0.187222</td>
      <td>0.177764</td>
      <td>0.173730</td>
      <td>0.165421</td>
      <td>0.189162</td>
      <td>...</td>
      <td>0.139265</td>
      <td>0.143008</td>
      <td>0.144231</td>
      <td>0.171584</td>
      <td>0.163796</td>
      <td>0.122038</td>
      <td>0.187739</td>
      <td>0.0</td>
      <td>0.177841</td>
      <td>0.137815</td>
    </tr>
    <tr>
      <th>36</th>
      <td>0.184343</td>
      <td>0.181869</td>
      <td>0.190323</td>
      <td>0.195639</td>
      <td>0.179730</td>
      <td>0.211128</td>
      <td>0.202568</td>
      <td>0.200530</td>
      <td>0.186916</td>
      <td>0.227041</td>
      <td>...</td>
      <td>0.195358</td>
      <td>0.175890</td>
      <td>0.163462</td>
      <td>0.213509</td>
      <td>0.191095</td>
      <td>0.165877</td>
      <td>0.273673</td>
      <td>0.0</td>
      <td>0.198291</td>
      <td>0.178092</td>
    </tr>
    <tr>
      <th>60</th>
      <td>0.198653</td>
      <td>0.212273</td>
      <td>0.233333</td>
      <td>0.239875</td>
      <td>0.200676</td>
      <td>0.228898</td>
      <td>0.207704</td>
      <td>0.208320</td>
      <td>0.228037</td>
      <td>0.234662</td>
      <td>...</td>
      <td>0.237911</td>
      <td>0.222404</td>
      <td>0.223291</td>
      <td>0.234472</td>
      <td>0.217420</td>
      <td>0.225118</td>
      <td>0.286809</td>
      <td>0.0</td>
      <td>0.215118</td>
      <td>0.212590</td>
    </tr>
    <tr>
      <th>hep</th>
      <td>0.178451</td>
      <td>0.169596</td>
      <td>0.154839</td>
      <td>0.207269</td>
      <td>0.175676</td>
      <td>0.189761</td>
      <td>0.172941</td>
      <td>0.193362</td>
      <td>0.193458</td>
      <td>0.182075</td>
      <td>...</td>
      <td>0.148936</td>
      <td>0.189199</td>
      <td>0.266026</td>
      <td>0.206522</td>
      <td>0.180370</td>
      <td>0.226303</td>
      <td>0.218938</td>
      <td>0.0</td>
      <td>0.172146</td>
      <td>0.205032</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 257 columns</p>
</div>




```python
# At this point, some TFs have 0 counts, so lets clean them out:
btodrop = set(bdf.T[bdf.sum() ==0].index.values)
dtodrop = set(ddf.T[ddf.sum() ==0].index.values)

todrop = btodrop.union(dtodrop)

bdf = bdf.drop(todrop, axis=1)
ddf = ddf.drop(todrop, axis=1)
```


```python
# the distributions of the various stages at this point:
for irow,row in ddf.iterrows():
    sns.kdeplot(row)
```


![png](img/output_25_0.png)



```python
# let's scale the counts in order to make stages comparable
ddf.loc[:,:] = preprocessing.scale(ddf.values, axis=1)
bdf.loc[:,:] = preprocessing.scale(bdf.values, axis=1)
```


```python
# the distributions after scaling:
for irow,row in ddf.iterrows():
    sns.kdeplot(row)
```


![png](output_27_0.png)


## The final table:
In the end, we have a scaled count per motif for each organism-stage.
For example the first 5 TF scaled counts in zebrafish-shield look like this:


```python
ddf.loc['shield',:].head()
```




    fam
    0    1.143067
    1    1.350141
    2   -0.201232
    3   -0.462287
    4   -0.440515
    Name: shield, dtype: float64



We can then get a corellation between any two cases


```python
scaled_counts_in_zebra_shield = ddf.loc['shield',:]
scaled_counts_in_amphi_hep = bdf.loc['hep',:]

# The first value is the pearson metric and the second is the metric's pvalue,
# how certain we are for the metric.
pearsonr(scaled_counts_in_zebra_shield.values, scaled_counts_in_amphi_hep.values)
```




    (-0.18212360335862862, 0.0044786665950091106)




```python
# We can make a table with pair-wise correlations for all stages
TABLE = pd.DataFrame()
for dstage in zebra_stages:
    for bstage in amphi_stages:
        TABLE.loc[dstage, bstage] = pearsonr(ddf.loc[dstage].values,bdf.loc[bstage].values)[0]
TABLE
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>8</th>
      <th>15</th>
      <th>36</th>
      <th>60</th>
      <th>hep</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>dome</th>
      <td>0.472447</td>
      <td>0.413344</td>
      <td>-0.020947</td>
      <td>-0.233493</td>
      <td>-0.217931</td>
    </tr>
    <tr>
      <th>shield</th>
      <td>0.427724</td>
      <td>0.620226</td>
      <td>0.304749</td>
      <td>-0.079934</td>
      <td>-0.182124</td>
    </tr>
    <tr>
      <th>80epi</th>
      <td>0.248738</td>
      <td>0.577668</td>
      <td>0.509119</td>
      <td>0.113895</td>
      <td>-0.064038</td>
    </tr>
    <tr>
      <th>8som</th>
      <td>-0.016492</td>
      <td>0.297138</td>
      <td>0.525042</td>
      <td>0.289232</td>
      <td>0.156917</td>
    </tr>
    <tr>
      <th>24h</th>
      <td>-0.091795</td>
      <td>0.182115</td>
      <td>0.427101</td>
      <td>0.263281</td>
      <td>0.264657</td>
    </tr>
    <tr>
      <th>48h</th>
      <td>-0.225320</td>
      <td>0.018198</td>
      <td>0.359862</td>
      <td>0.292274</td>
      <td>0.196003</td>
    </tr>
  </tbody>
</table>
</div>



We can visualize the table in a heatmap:


```python
sns.set_style('white')
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
```


```python

fig, ax = plt.subplots(figsize=(16,9))
sns.heatmap(TABLE,
            annot=True,
            cmap="RdBu_r",
            vmax=0.66, vmin=-0.66,
            ax=ax
           )

ax.set_title("TF Correlation")
```




    Text(0.5,1,u'TF Correlation')




![png](output_35_1.png)


Or we can plot the minimum distance per amphioxus stage, to show the hourglass behaviour


```python
x = [ 0, 1, 2, 3, 4]
labels = ['8h','15h','36h','60h','hepatic']
```


```python
fig, ax = plt.subplots(figsize=(27,9))

ax.plot( TABLE.max().values, linewidth=2)

ax.set_xticks(x)
ax.set_xticklabels(labels)

ax.tick_params(direction ='out',
               length=10, width=3, colors='black',labelsize='xx-large')
ax.invert_yaxis()
```


![png](output_38_0.png)

